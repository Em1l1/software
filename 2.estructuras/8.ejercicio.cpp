/*
* Ejercicio 8: Escribe un programa que lea de la entrada
* estandar tres numeros. Despues debe ler un 
* cuarto e indicar si el numero coincide con alguno de los 
* introducidos con anterioridad.
*/

#include <iostream>
using namespace std;

int main() {
  int n1, n2, n3, n4;

  cout << "Digite 4 numeros." << endl;
  cout << "Digite el primer numero: "; cin >> n1;
  cout << "Digite el segundo numero: "; cin >> n2;
  cout << "Digite el tercer numero: "; cin >> n3;
  cout << "\nDigite el cuarto numero: "; cin >> n4;

  if ((n1 == n4) || (n2 == n4) || (n3 == n4)) {
    cout << "\nEl cuarto numero conincide con almenos 1 de los 3 anteriores." << endl;
  } else {
    cout << "El numero cuarto introducido no coincide con los anteriores."  << endl;
  }

  return 0;
}
