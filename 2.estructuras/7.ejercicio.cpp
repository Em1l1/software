/*
 * Ejercicio 7: Escriba un programa que solicite una
 * edad (Entero) e indique en la salida
 * estandar si la edad introducida esta en el 
 * rango de [18 - 25]
 * */

#include <iostream>
using namespace std;

int main() {
  int edad;

  cout << "Digite la edad: "; cin >> edad;

  if ((edad > 17) && (edad < 26)) {
    cout << "\nLa edad esta en el rango de [18 - 25]." << endl;
  } else {
    cout << "La edad no esta en dicho rango.";
  }

  return 0;
}
