/*
 * Ejercicio 4: Comprobar si un numero digitado por el usuario es
 * positivo o negativo
 *
 * */

#include <iostream>

using namespace std;

int main() {
  int numero;

  cout << "Digite un numero: "; cin >> numero;
  if (numero < 0) {
    cout << endl << "El numero ingresado es negativo" << endl;
  }

  if (numero > 0) {
    cout << endl << "El numero ingresado es positivo" << endl;
  }

  if (numero == 0) {
    cout << endl << "El numero es cero" << endl;
  }

  return 0;
}


