/*
* La sentencia for (i = 0; i < length; i++) {
 conjunto de expresiones; 
}
*/

#include <iostream>
using namespace std;

int main() {
  // incremento
  int i;
  for(i = 1; i <= 10; i++) {
    cout << i << endl;
  }

  // Decremento
  cout << "\n.:Decremento:." << endl;
  int j;
  for (j = 10; j >= 1; j--) {
    cout << j << endl;
  }

  return 0;
}
