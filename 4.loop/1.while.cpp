/*
* La sentencia while:
*
* while (expresion logica) {
*   conjunto de instrucciones
* }
*
*/

#include <iostream>
#include <conio.h>

using namespace std;

int main() {

  int i,j;

  i = 1;
  j = 10;
  while (i <= 10) {
    cout << i << endl;
    i++;
  }

  while (j>= 1) {
    cout << j << endl;
    j--;
  }

  return 0;
}
