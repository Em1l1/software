/*
 * if (condition) {
 *  code
 * } else {
 * code
 * }
 *
 * == igual
 * != diferente
 * > mayor
 * < menor
 * >= mayor o igual
 * <= menor o igual
 */

#include <iostream>

using namespace std;

int main() {
  int numero, dato = 5;

  cout << "Digite un numero: ";
  cin >> numero;

  if (numero != dato) {
    cout << "El numero no es 5" << endl;
  } else {
    cout << "El numero es 5" << endl;
  }
  return 0;
}
