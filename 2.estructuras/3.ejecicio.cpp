/*
 * Ejercico 3: Realice un programa que lea un
 * valor entero y deternmine
 * si se trata de un numero par o impar
*/

#include <iostream>

using namespace std;

int main() {
  int numero;

  cout << "Digite el numero: ";
  cin >> numero;
  if (numero == 0) {
    cout << "El numero es cero";
  }
  else if (numero % 2 == 0) {
    cout << "\nEs un numero PAR" << endl;
  } else {
    cout << "\nEl numero es IMPAR" << endl;
  }
  return 0;
}
