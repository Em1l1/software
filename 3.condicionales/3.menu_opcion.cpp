/*
* Ejercicio menu: Hacer un menu que considerer las 
* siguientes opciones
*   caso 1: cubo de un numero
*   caso 2: Numero par o impar
*   caso 3: salir
*/

#include <iostream>
#include <cmath>
using namespace std;

int main() {

  int opcion;
  cout << "\t.:Menu Math:." << endl;
  cout << "\t1. Cubo de un numero" << endl;
  cout << "\t2. Numero par o impar" << endl;
  cout << "\t3. Salir" << endl;

  cout << "Digite una opcion: ";
  cin >> opcion;

    
    switch(opcion) {
      case 1:
        cout << endl << endl << endl << "Cubo de un numero" << endl << endl << endl;
        int numero;
        cout << "Digita el numero a elevar al cubo: ";
        cin >> numero;
        cout << endl << endl << "El cubo del " << numero << " es " << pow(numero, 3) << endl;
        
        break;
      case 2: 
        cout << endl << endl << endl << "Numero par o impart" << endl << endl << endl;
        int n1;

        cout << "Digita un numero: ";
        cin >> n1;

        if (n1 % 2 == 0) {
          cout << "\nEl numero ingresado es par." << endl;
      } else {
        cout << "\nEl numero ingresado es Impar" << endl;
      }
        break;
      case 3: 
        cout << "Salir....." << endl << endl << endl;
        break;
    }


  return 0;
}
