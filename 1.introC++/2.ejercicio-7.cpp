/*
Ejercicio 7: La calificacion final de un estudiante es el 
promedio de tres notas

- La nota de practicas que cuenta un 30% del total
- La nota teorica que cuenta con 60%
- La nota de participacion qu ecuenta el 10% restante

Escriba un programa que lea las tres notas del alumno y escriba su nota final
*/

#include <iostream>
using namespace std;

int main() {
  float nota1, nota2, nota3, promedio;

  cout << "Digite la nota de practicas: "; cin >> nota1;
  cout << "Digite la nota teorica: "; cin >> nota2;
  cout << "Digite la nota de participacion: "; cin >> nota3;

  nota1 *= 0.30;
  nota2 *= 0.60;
  nota3 *= 0.10;

  cout.precision(3);
  promedio = nota1 + nota2 + nota3;
  cout << "\n\nEl promedio de las notas es: " << promedio << endl;

  return 0;

}