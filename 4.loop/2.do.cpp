/*
* La sentencia do {
   conjunto de instrucciones 
} while (expresion logica);
*/

#include <iostream>
// include <stdlib.h> // system("pause");
using namespace std;

int main() {
  int i;

  i = 1;

  do {
    cout << i << endl;
    i++;                // aumenta el iterador de uno en uno
  } while (i <= 10);

  int j;

  j = 10;

  do {
    cout << j << endl;
    j--; // decrementa el iterador de uno en uno
  } while(j >= 1);
  return 0;
}
