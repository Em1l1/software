/*
 * Escribe un programa que lea de la entrada estandar el precio de un 
 * producto y muestre en la salida estnadar
 * el precio del producto al
 * aplicarle el IVA.
 * */

#include <iostream>

using namespace std;

int main() {
  float precio, IVA, precioFinal;
  cout << "Digite el preico del producto: "; cin >> precio;
  
  IVA = precio * 0.21;
  precioFinal = precio + IVA;

  cout << endl << "El precio del producto al aplicarle el IVA es: " << " Q " 
    << precioFinal << endl;

  cout << "El IVA es de : Q. " << IVA << endl;
  return 0;
}
