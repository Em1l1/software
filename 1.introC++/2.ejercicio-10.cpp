/*
Ejercicio 10: Escriba un programa que calcule las soluciones de una ecuacion
de segundo grado de la forma ax^2 + bx + c = 0
teniendo en cuenta que :
x = -b +- raiz cuadrada de b2 - 4ac / 2a
*/

#include <iostream>
#include <cmath>

using namespace std;

int main() {
  float a, b, c, resultado = 0, result = 0;

  cout << "Digite el valor de a: "; cin >> a;
  cout << "Digite el valor de b: "; cin >> b;
  cout << "Digite el valor de c: "; cin >> c;

  resultado = (- b + (sqrt(pow(b,2) - (4 * a * c)))) / (2 * a);
  result = (- b - (sqrt(pow(b,2) - (4 * a * c)))) / (2 * a);

  cout.precision(3);
  cout << "\nEl resultado 1 es: " << resultado << endl;
  cout << "El resultado 2 es: " << result << endl;
  return 0;
}
