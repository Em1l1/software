/*
 * Escribe la siguiente expresion como expresion en c++
 * a / b + 1
 * 
 */

#include <iostream>
using namespace std;

int main() {
  float a, b, resultado = 0;

  cout << "Digite el valor de a: "; cin >> a;
  cout << "Digite el valor de b: "; cin >> b;
  resultado = (a/b) + 1;

  // Fucion de preccion de decimales, solo se muestran 4 decimales
  cout.precision(3);
  cout << "El resultado es: " << resultado << endl;
  return 0;
}
