/*
* Ejercicio 2: Realice un programa que lea la entrada estandar
* numeros hasta que se introduzca un cero. En ese momento el 
* programa debe terminar y mostrar en la salida estandar 
* el numero de valores mayores que cero leidos.
*/
#include <iostream>
using namespace std;

int main() {

  int numero, contador = 0;

  do {
    cout << ".: "<< "Digite un numero: "; cin >> numero;
    // contador
    if (numero > 0) {
      contador++;
    }
  } while(numero != 0);

  cout << "\n.:El numero de valores mayores a cero introducidos es: " << contador << endl;
  return 0;
}
