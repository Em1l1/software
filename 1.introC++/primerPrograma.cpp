/*
 * 1. Escribe un programa que lea de la entrada estandar dos numeros y muestre en Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit enim labore culpa sint ad nisi Lorem pariatur mollit ex esse exercitation amet. Nisi anim cupidatat excepteur officia. Reprehenderit nostrud nostrud ipsum Lorem est aliquip amet voluptate voluptate dolor minim nulla est proident. Nostrud officia pariatur ut officia. Sit irure elit esse ea nulla sunt ex occaecat reprehenderit commodo officia dolor Lorem duis laboris cupidatat officia voluptate. Culpa proident adipisicing id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. Aliqua reprehenderit commodo ex non excepteur duis sunt velit enim. Voluptate laboris sint cupidatat ullamco ut ea consectetur et est culpa et culpa duis.
 * salida estandar su suma, resta, multipliacion y division
 * */
#include <iostream>

using namespace std;

int main() {
  int n1, n2, suma = 0, resta = 0, multiplicacion = 0, division = 0;

  cout << "Digite un numero: "; cin >> n1;
  cout << "Digite otro numeor: "; cin >> n2;

  suma = n1 + n2;
  resta = n1 - n2;
  multiplicacion = n1 * n2;
  division = n1 / n2;


  cout << "\nLa suma es: " << suma << endl;
  cout << "La resta es: " << resta << endl;
  cout << "La multiplicacion es: " << multiplicacion << endl;
  cout << "La division es: " << division << endl;
  return 0;
}
