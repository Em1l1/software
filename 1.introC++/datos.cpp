// Tipos de Datos basicos en C++

#include <iostream>
using namespace std;

int main() {
  int entero = 15;
  float floatante = 19.43;
  double mayor = 14.23423;
  char letra = 'a';

  cout << entero << endl;
  cout << floatante << endl;
  cout << mayor << endl;
  cout << letra << endl;


  return 0;
}
