/*
 * Ejercicio 1: Escriba un programa que lea dos numeros y determine cual de ellos es el mayor
 * */

#include <iostream> 
using namespace std; 
int main() { 
  int n1, n2, n3;
  cout << "Digite el primer numero: ";
  cin >> n1;
  cout << "Digite el segundo numero: ";
  cin >> n2;
  cout << "Digite el tercer numero: ";
  cin >> n3;

  if ((n1 >= n2) && (n2 >= n3)) {
    cout << "\nEl mayor es: " << n1 << endl;
  } else if ((n2 >= n1) && (n2 >= n3)) {
    cout << "\nEl mayor es: " << n2 << endl;
  } else {
    cout << "\nEl mayor es: " << n3 << endl;
  }

  return 0;
}
