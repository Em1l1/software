/*
 * Escribe la siguiente expresion matematica como expresion en C++ 
 * a + b / c - d
*/

#include <iostream>

using namespace std;

int main(){
  float a, b, c, d, resultado = 0;

  cout << "Digitalice el valor de a: "; cin >> a;
  cout << "Digitalice el valor de b: "; cin >> b;
  cout << "Digitalice el valor de c: "; cin >> c;
  cout << "Digitalice el valor de d: "; cin >> d;
  
  resultado = a + (b/(c-d));

  cout.precision(3);
  cout << "\n\nEl resultado es: " << resultado << endl;

  return 0;
}
