/*
Ejercicio 9: Realice un programa que calcule el valor que toma la siguiente funcion para valores dados de x y y.

f (x,y) = raiz cuadata x / y al cuadrado - 1
*/
#include <iostream>
#include <cmath>
using namespace std;

int main() {
  float x, y, result, res;

  cout << "Digite el valor de x: "; cin >> x;
  cout << "Digite el valor de y: "; cin >> y;

  result = x * y;

  res = (sqrt(x) / (pow(y, 2) - 1));

  cout << "\n\nEl resultado de los valores es: " << res << endl;


  return 0;
}