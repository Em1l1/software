/*
Escribe la siguiente expresion matematica como expresion en c++
* (a+b)/(c+d)
*/

#include <iostream>
using namespace std;

int main() {
  float a, b, c, d, resultado = 0;

  cout << "Digite el valora de a: "; cin >> a;
  cout << "Digite el valora de b: "; cin >> b;
  cout << "Digite el valora de c: "; cin >> c;
  cout << "Digite el valora de d: "; cin >> d;

  resultado = (a + b) / (c + d);

  cout.precision(3);
  cout << "La expresion matematica es: " << resultado << endl;
  return 0;
}
