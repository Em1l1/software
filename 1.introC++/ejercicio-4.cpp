/*
 * Ejercicio 4: Ejecute el ejercicio anterior conentradas erroneas y 
 * observe los resultados. Por ejemplo, introduzca un dato de tipo Caracter
 * cuando se espera un dato de tipo entero
 *
 * @Edad: dato de tipo de entero.
 * @Sexo: dato de tipo Caracter.
 * @Altura en metros: dato de tipo real.
 *
 * Tras leer los datos, el programa debe mostrarlos en la salida estandar.
 *
*/
#include <iostream>

using namespace std;

int main() {
  int edad;
  float altura;
  char sexo[10];


  cout << "Ingrese la edad: "; cin >> edad;
  cout << "Ingrese su sexo: "; cin >> sexo;
  cout << "Ingrese su altura: "; cin >> altura;

  cout << "\n\nEdad: " << edad << endl;
  cout << "Sexo: " << sexo << endl;
  cout << "Altura: " << altura << " mts" << endl;

  return 0;
}
