/*
Ejercicio 8: Escriba un programa que lea de la entrada estandar los dos catetos de un triangulo rectangulo
y escriba en la salida estandar su hipotenusa.
*/
#include <iostream>
#include <cmath>
using namespace std;
int main() {
  float a, b, hipotenusa, resultado = 0;

  cout << "Digite la primera medida: "; cin >> a;
  cout << "Digite la segunda medida: "; cin >> b;

//  a *= a;
//  b *= b;

 // hipotenusa = a + b;
 resultado = sqrt(pow(a,2) + pow(b,2));

  cout.precision(3);
  // resultado = sqrt(hipotenusa);

  cout << "\n\nLa medida de la hipotenusa del triangulo es: " << resultado << endl;
  return 0;
}