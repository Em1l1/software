/*
* ejercicio 1: Hacer un programa que simule un cajero automatico con un saldo inicial
* de 1000 dolares
*/
#include <iostream>
using namespace std;

int main() {
  int opcion, deposito;
  int saldoInicial= 1000;

  do {
    cout << "\n\tCajero automatico" << endl;
    cout << "\t\tMenu"<< endl;
    cout << "\t1. Depositar" << endl;
    cout << "\t2. Retirar" << endl;
    cout << "\t3. Verificar Cuenta" << endl;
    cout << "\t4. Salir" << endl;
    cout << "\n\tDigite una Opcion: "; cin >> opcion;

    switch(opcion) {
      case 1:
        cout << "\tDepositar efectivo." << endl << endl;
        cout << "\tDigite el efectivo a despositar: $ ";
        cin >> deposito;
        cout << endl << endl << "Desposito realizado con exito....." << endl;
        saldoInicial += deposito;
        break;
      case 2:
        cout << "\tRetirar efectivo." << endl << endl;
        cout << "\tDigite el monto a Retirar: $ ";
        cin >> deposito;

        if (deposito > saldoInicial) {
          cout << endl << endl << "No tiene esa cantidad de dinero" << endl;
        } else {
          saldoInicial -= deposito;
          cout << endl << endl << "\tEl saldo actual es de: $ " << saldoInicial << endl;
        }
        break;
      case 3:
        cout << "\tConsultar Saldo." << endl << endl;
        cout << "\tEl saldo Acutal es de: $ " << saldoInicial << endl;
        break;
      case 4:
        cout << "\tSaliendo......" << endl;
        break;
    }

  } while(opcion != 4);

  return 0;
}
